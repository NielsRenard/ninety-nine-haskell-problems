# ninety-nine-haskell-problems

Solutions to `https://wiki.haskell.org/H-99:_Ninety-Nine_Haskell_Problems`.

"This is part of Ninety-Nine Haskell Problems, based on Ninety-Nine Prolog Problems and Ninety-Nine Lisp Problems. "