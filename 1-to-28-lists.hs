import           Data.Char
import           Data.List     (group)

{-
  Problem 1
  (*) Find the last element of a list.
  λ> myLast [1,2,3,4]
  4
  λ> myLast ['x','y','z']
  'z'
-}

myLast :: [a] -> a
myLast []     = error "empty list"
myLast [a]    = a
myLast (x:xs) = myLast(xs)


{-
  Problem 2
  (*) Find the last but one element of a list.
  λ> myButLast [1,2,3,4]
  3
  λ> myButLast ['a'..'z']
  'y'
-}


myButLast :: [c] -> c
myButLast = last . init
myButLast2  =  head . tail  . reverse
myButLast3 x = (!!) x . pred . pred $ length x


{-
  (*) Find the K'th element of a list.
  The first element in the list is number 1.
  Example:
  * (element-at '(a b c d e) 3)
  c
  Example in Haskell:
  λ> elementAt [1,2,3] 2
  2
  λ> elementAt "haskell" 5
  'e'
-}


elementAt :: [a] -> Int -> a
elementAt xs n = xs !! pred n

{-
  in clojure
  (defn element-at [xs n]
    (get xs (- n 1))
-}

{-
  Problem 4
  (*) Find the number of elements of a list.
  Example in Haskell:
  λ> myLength [123, 456, 789]
  3
  λ> myLength "Hello, world!"
  13
-}

myLength (x:xs) = length (x:xs)

{-
  Problem 5
  (*) Reverse a list.
  Example in Haskell:
  λ> myReverse "A man, a plan, a canal, panama!"
  "!amanap ,lanac a ,nalp a ,nam A"
  λ> myReverse [1,2,3,4]
  [4,3,2,1]
-}

myReverse = reverse

{-
  Problem 6
  (*) Find out whether a list is a palindrome.
  A palindrome can be read forward or backward; e.g. (x a m a x).
  Example in Haskell:
  λ> isPalindrome [1,2,3]
  False
  λ> isPalindrome "madamimadam"
  True
  λ> isPalindrome [1,2,4,8,16,8,4,2,1]
  True
-}


isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome a = a == reverse a

{-
  (**) Flatten a nested list structure.
  Transform a list, possibly holding lists as elements into a `flat' list by replacing each list with its elements (recursively).
  Example:
  * (my-flatten '(a (b (c d) e)))
   (A B C D E)
  Example in Haskell:
  We have to define a new data type NestedList, because lists in Haskell are homogeneous.
  i.e. : [1, [2, [3, 4], 5]] is a type error. Therefore, we must have a way of representing a list that may (or may not) be nested.
  data NestedList a = Elem a | List [NestedList a]
  λ> flatten (Elem 5)
  [5]
  λ> flatten (List [Elem 1, List [Elem 2, List [Elem 3, Elem 4], Elem 5]])
  [1,2,3,4,5]
  λ> flatten (List [])
  []
  Our NestedList datatype is either a single element of some type (Elem a), or a list of NestedLists of the same type. (List [NestedList a]).
-}

data NestedList a = Elem a | List [NestedList a]

flatten :: NestedList a -> [a]
flatten (List [])          = []
flatten (Elem x)           = [x]
flatten (List (head:tail)) = flatten head ++ flatten (List tail)


{-
  Problem 8
  (**) Eliminate consecutive duplicates of list elements.
  If a list contains repeated elements they should be replaced with a single copy of the element. The order of the elements should not be changed.
  Example:
  * (compress '(a a a a b c c a a d e e e e))
  (A B C A D E)
  Example in Haskell:
  λ> compress "aaaabccaadeeee"
  "abcade"
-}

compress :: Eq a => [a] -> [a]
compress [] = []
compress (x:xs)
  | null xs            = [x]
  | x == head xs       = compress . (++) [x] $ tail xs
  | otherwise          = (++) [x] (compress xs)


{-
  Problem 9
  (**) Pack consecutive duplicates of list elements into sublists. If a list contains repeated elements they should be placed in separate sublists.
  Example:
  * (pack '(a a a a b c c a a d e e e e))
  ((A A A A) (B) (C C) (A A) (D) (E E E E))
  Example in Haskell:
  λ> pack ['a', 'a', 'a', 'a', 'b', 'c', 'c', 'a','a', 'd', 'e', 'e', 'e', 'e']
  ["aaaa","b","cc","aa","d","eeee"]
-}

pack :: Eq a => [a] -> [[a]]
pack x = Data.List.group x


{-
  Problem 10
  (*) Run-length encoding of a list. Use the result of problem P09 to implement the so-called run-length encoding data compression method. Consecutive duplicates of elements are encoded as lists (N E) where N is the number of duplicates of the element E.
  Example:
  * (encode '(a a a a b c c a a d e e e e))
  ((4 A) (1 B) (2 C) (2 A) (1 D)(4 E))
  Example in Haskell:
  λ> encode "aaaabccaadeeee"
  [(4,'a'),(1,'b'),(2,'c'),(2,'a'),(1,'d'),(4,'e')]
-}

-- this only works for tuples
encode :: Eq a => [a] -> [(Int, a)]
encode x = map encodeHelper $ group x
  where
    encodeHelper x = (length x, head x)

-- more natural way to do this
twoFuns :: (a -> b ) -> (a -> c) -> a -> (b, c)
twoFuns f g x = (f x, g x)


{-
  Problem 11
  (*) Modified run-length encoding.
  Modify the result of problem 10 in such a way that if an element has no duplicates it is simply copied into the result list. Only elements with duplicates are transferred as (N E) lists.
  Example:
  * (encode-modified '(a a a a b c c a a d e e e e))
  ((4 A) B (2 C) (2 A) D (4 E))
  Example in Haskell:
  λ> encodeModified "aaaabccaadeeee"
  [Multiple 4 'a',Single 'b',Multiple 2 'c',
   Multiple 2 'a',Single 'd',Multiple 4 'e']
-}


-- didn't use types, just returned strings
encodeModified' x
  | length x == 1 = "Single " ++ x
  | length x > 1 = "Multiple " ++ show (length x) ++ " " ++ show (head x)

answer11 = map encodeModified' $ group "aaaabccaadeeee"


-- the example solution using types

data ListItem a = Single a | Multiple Int a
  deriving (Show) -- this allows your datatype to be printed

encodeModified :: Eq a => [a] -> [ListItem a]
encodeModified = map encodeHelper . encode
    where -- this helper fn uses our encode fn from problem 10
      encodeHelper (1,x) = Single x
      encodeHelper (n,x) = Multiple n x

{-
  Problem 12
  (**) Decode a run-length encoded list.
  Given a run-length code list generated as specified in problem 11. Construct its uncompressed version.
  Example in Haskell:
  λ> decodeModified
         [Multiple 4 'a',Single 'b',Multiple 2 'c',
          Multiple 2 'a',Single 'd',Multiple 4 'e']
  "aaaabccaadeeee"
-}

decodeModified :: [ListItem a] -> [a]
decodeModified = concat . map decodeHelper
  where decodeHelper (Multiple n x)  = replicate n x
        decodeHelper (Single x) = [x]


{-
  Problem 13
  (**) Run-length encoding of a list (direct solution).
  Implement the so-called run-length encoding data compression method directly. I.e. don't explicitly create the sublists containing the duplicates, as in problem 9, but only count them. As in problem P11, simplify the result list by replacing the singleton lists (1 X) by X.
  Example:
  * (encode-direct '(a a a a b c c a a d e e e e))
  ((4 A) B (2 C) (2 A) D (4 E))
  Example in Haskell:
  λ> encodeDirect "aaaabccaadeeee"
  [Multiple 4 'a',Single 'b',Multiple 2 'c',
   Multiple 2 'a',Single 'd',Multiple 4 'e']
-}


{-
  Problem 14
  (*) Duplicate the elements of a list.
  Example:
  * (dupli '(a b c c d))
  (A A B B C C C C D D)
  Example in Haskell:
  λ> dupli [1, 2, 3]
  [1,1,2,2,3,3]
-}


dupli :: [a] -> [a]
dupli [] = []
dupli (x:xs) = x:x:(dupli xs)
-- single colon is a list constructor

{-
  Problem 15
  (**) Replicate the elements of a list a given number of times.
  Example:
  * (repli '(a b c) 3)
  (A A A B B B C C C)
  Example in Haskell:
  λ> repli "abc" 3
  "aaabbbccc"
-}

repli :: [a] -> Int -> [a]
repli [] n = []
repli (x:xs) n = replicate n x <> repli xs n

{-
  Problem 16
  (**) Drop every N'th element from a list.
  Example:
  * (drop '(a b c d e f g h i k) 3)
  (A B D E G H K)
  Example in Haskell:
  λ> dropEvery "abcdefghik" 3
  "abdeghk"
-}

dropEvery :: [a] -> Int -> [a]
dropEvery [] n = []
dropEvery (xs) n = take (pred n) xs <> dropEvery (drop n xs) n

{-
  Problem 17
  (*) Split a list into two parts; the length of the first part is given.
  Do not use any predefined predicates.
  Example:
  * (split '(a b c d e f g h i k) 3)
  ( (A B C) (D E F G H I K))
  Example in Haskell:
  λ> split "abcdefghik" 3
  ("abc", "defghik")
-}

split :: [a] -> Int -> ([a], [a])
split xs n = (take n xs, drop n xs)

{-
  Problem 18
  (**) Extract a slice from a list.
  Given two indices, i and k, the slice is the list containing the elements between the i'th and k'th element of the original list (both limits included). Start counting the elements with 1.
  Example:
  * (slice '(a b c d e f g h i k) 3 7)
  (C D E F G)
  Example in Haskell:
  λ> slice ['a','b','c','d','e','f','g','h','i','k'] 3 7
  "cdefg"
-}

slice :: [a] -> Int -> Int -> [a]
slice xs n p = drop (pred n) (take p xs)

{-
  Problem 19
  (**) Rotate a list N places to the left.
  Hint: Use the predefined functions length and  (++).
  Examples:
  * (rotate '(a b c d e f g h) 3)
  (D E F G H A B C)
  * (rotate '(a b c d e f g h) -2)
  (G H A B C D E F)
  Examples in Haskell:
  λ> rotate ['a','b','c','d','e','f','g','h'] 3
  "defghabc"
  λ> rotate ['a','b','c','d','e','f','g','h'] (-2)
  "ghabcdef"
-}

rotate :: [a] -> Int -> [a]
rotate xs n = take xsLength . drop (xsLength + n) $ cycle xs
    where xsLength = (length xs)

{-
  Problem 20
  (*) Remove the K'th element from a list.
  Example in Prolog:
  ?- remove_at(X,[a,b,c,d],2,R).
  X = b
  R = [a,c,d]
  Example in Lisp:
  * (remove-at '(a b c d) 2)
  (A C D)
  (Note that this only returns the residue list, while the Prolog version also returns the deleted element.)
  Example in Haskell:
  λ> removeAt 2 "abcd"
  ('b',"acd")
-}

removeAt :: Int -> [a] -> (Maybe a, [a])
removeAt n xs
  | n > 0 && n <= length xs = ( Just $ xs !! pred n, take (pred n) xs <> (drop n xs))
  | otherwise = (Nothing, xs)

{-
  Problem 21
  Insert an element at a given position into a list.
  Example:
  * (insert-at 'alfa '(a b c d) 2)
  (A ALFA B C D)
  Example in Haskell:
  λ> insertAt 'X' "abcd" 2
  "aXbcd"
-}

insertAt :: a -> [a] -> Int -> [a]
insertAt x xs n = left ++ middle ++ right
  where (left, right) = split xs (pred n)
        middle = [x]

{-
  Problem 22
  Create a list containing all integers within a given range.
  Example:
  * (range 4 9)
  (4 5 6 7 8 9)
  Example in Haskell:
  λ> range 4 9
  [4,5,6,7,8,9]
-}

range :: Enum a => a -> a -> [a]
range n m = [n..m]

{-
  Problem 23
  Extract a given number of randomly selected elements from a list.
  Example:
  * (rnd-select '(a b c d e f g h) 3)
  (E D A)
  Example in Haskell:
  λ> rnd_select "abcdefgh" 3 >>= putStrLn
  eda
-}

{-
  Problem 24
  Lotto: Draw N different random numbers from the set 1..M.
  Example:
  * (rnd-select 6 49)
  (23 1 17 33 21 37)
  Example in Haskell:
  λ> diff_select 6 49
  [23,1,17,33,21,37]
-}
